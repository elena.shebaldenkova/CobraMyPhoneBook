***Создание проекта***

* На пк должен быть установлен GO 
1. Установить cobra -> cmd -> go get github.com/spf13/cobra/cobra
2. Создание(инициализация) приложения -> консоль -> переход в директорию %GOPATH%\bin -> cobra init  %GOPATH%\src\cobra-example
3. Создание команд -> консоль -> директория проекта -> cobra add [имя команды, которая будет создана]   
4. Вызов команды ->  консоль -> директория проекта ->  go run main.go [имя созданной команды]





***Работа с приложением***


Скачать проект - go get gitlab.com/elena.shebaldenkova/CobraMyPhoneBook.git

Перейти в терминале в папку  %GOPATH%/src/gitlab.com/elena.shebaldenkova/CobraMyPhoneBook.git

Выполнить команду  -  go run main.go [выбранную команду], в зависимости от желаемых действий:

***Перечень доступных команд*** 

 - createFile - создать файл

 - deleteFile - удалить файл 

 - addRecordByPhone - добавить запись в телефонную книгу по номеру телефона 

 - viewAllRecord - просмотр всех записей из телефонной книги 

 - findRecordByPhone - найти запись в телефонной книге по номеру телефона

 - deleteRecordByPhone - удалить запись из телефонной книги по номеру телефона

 - updateNameByPhone - обновить имя в телефонной книге по номеру телефона 

 - updateAdressByPhone - обновить адрес в телефонной книге по номеру телефона

 - listCommands - перечень возможных команд 