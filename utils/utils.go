package utils

import (
	"fmt"
	"regexp"
)

//валидация введенного номера соответствующему формату +380501234567
func CheckValidNumber(key string) bool {
	var validNumber = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`)
	val := validNumber.MatchString(key)
	return val
}

func IsError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}
