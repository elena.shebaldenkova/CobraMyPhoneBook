package models

type Record struct {
	Id     string
	Name   string
	Adress string
}

func RecordsToString(record Record) string {
	return record.Id + " " + record.Name + " " + record.Adress + "\n"
}
