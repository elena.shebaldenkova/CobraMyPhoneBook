package cmd

import (
	"cobraPhonebook/models"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/spf13/cobra"
)

// метод удаляет запись по номеру телефона
var deleteRecordByPhoneCmd = &cobra.Command{
	Use: "deleteRecordByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		//var key string
		r := new(models.Record)
		fmt.Print("Для удаления существующей записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Id)
		file, _ := ioutil.ReadFile(nameCreatedFile)

		lines := strings.Split(string(file), "\n")
		for i, line := range lines {
			if strings.Split(line, " ")[0] == r.Id {
				lines[i] = lines[len(lines)-1]
				lines = lines[:len(lines)-1]
				fmt.Println("Запись под номером " + r.Id + " удалена успешно")
				toFile := strings.Join(lines, "\n")
				ioutil.WriteFile(nameCreatedFile, []byte(toFile), 0644)
				return
			}
		}
		fmt.Println("Искомая запись отсутствует ")
	},
}

func init() {
	rootCmd.AddCommand(deleteRecordByPhoneCmd)
}
