package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// метод выводит перечень всех команд
var listCommandsCmd = &cobra.Command{
	Use: "listCommands",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Ведите go run main.go и допишите желаемое действие:\n \tcreateFile - создать файл \n \taddRecordByPhone - cоздать новую запись\n \tfindRecordByPhone - найти запись по номеру телефона\n \tupdateNameByPhone - обновить имя по номеру телефона\n \tupdateAdressByPhone - обновить адрес по номеру телефона\n \tviewAllRecord - просмотр всех записей\n \tdeleteRecordByPhone - удалить запись по номеру телефона\n \tdeleteFile - удалить файл\n")
	},
}

func init() {
	rootCmd.AddCommand(listCommandsCmd)
}
