package cmd

import (
	"cobraPhonebook/models"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/spf13/cobra"
)

// метод находит запись по номеру телефона
var findRecordByPhoneCmd = &cobra.Command{
	Use: "findRecordByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		r := new(models.Record)
		fmt.Print("Для поиска существующей записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Id)
		file, _ := ioutil.ReadFile(nameCreatedFile)
		lines := strings.Split(string(file), "\n")
		for _, line := range lines {
			if strings.Split(line, " ")[0] == r.Id {
				fmt.Println("Запись найдена - " + line)
				return
			}

		}
		fmt.Println("Искомая запись отсутствует")
	},
}

func init() {
	rootCmd.AddCommand(findRecordByPhoneCmd)
}
