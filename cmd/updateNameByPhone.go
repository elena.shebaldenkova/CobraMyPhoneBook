package cmd

import (
	"cobraPhonebook/models"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/spf13/cobra"
)

// метод обновляет поле Имя записи, найденной в телефонной книге по номеру телефона
var updateNameByPhoneCmd = &cobra.Command{
	Use: "updateNameByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		r := new(models.Record)
		fmt.Print("Для изменения существующей записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Id)
		file, _ := ioutil.ReadFile(nameCreatedFile)
		lines := strings.Split(string(file), "\n")
		for i, line := range lines {
			if strings.Split(line, " ")[0] == r.Id {
				fmt.Print("Введите новое имя ")
				fmt.Scanln(&r.Name)
				var adress = strings.Split(line, " ")[2] + " " + strings.Split(line, " ")[3] + " " + strings.Split(line, " ")[4] + " " + strings.Split(line, " ")[5] + "\n"
				r := models.Record{
					Id:     r.Id,
					Name:   r.Name,
					Adress: adress,
				}
				lines[i] = models.RecordsToString(r)
				toFile := strings.Join(lines, "\n")
				ioutil.WriteFile(nameCreatedFile, []byte(toFile), 0644)
				fmt.Println("Запись обновлена - " + models.RecordsToString(r))
				return
			}
		}
		fmt.Println("Искомая запись отсутствует ")

	},
}

func init() {
	rootCmd.AddCommand(updateNameByPhoneCmd)
}
