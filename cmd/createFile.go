package cmd

import (
	"cobraPhonebook/utils"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var nameCreatedFile = "MyPhoneBook.txt"

// метод создает файл
var createFileCmd = &cobra.Command{
	Use: "createFile",

	Run: func(cmd *cobra.Command, args []string) {
		var _, err = os.Stat(nameCreatedFile)
		if os.IsNotExist(err) {
			var file, err = os.Create(nameCreatedFile)
			if utils.IsError(err) {
				return
			}
			defer file.Close()
			fmt.Println("файл " + nameCreatedFile + " создан")
		} else {
			fmt.Println("файл " + nameCreatedFile + " уже существует")
		}
	},
}

func init() {
	rootCmd.AddCommand(createFileCmd)
}
