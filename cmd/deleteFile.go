package cmd

import (
	"cobraPhonebook/utils"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// метод удаляет файл
var deleteFileCmd = &cobra.Command{
	Use: "deleteFile",
	Run: func(cmd *cobra.Command, args []string) {
		var err = os.Remove(nameCreatedFile)
		if utils.IsError(err) {
			return
		}
		fmt.Println("Файл " + nameCreatedFile + " удален")
	},
}

func init() {
	rootCmd.AddCommand(deleteFileCmd)
}
