package cmd

import (
	"bufio"
	"cobraPhonebook/models"
	"cobraPhonebook/utils"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

// метод добавляет запись в справочник
var addRecordByPhoneCmd = &cobra.Command{
	Use: "addRecordByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		r := new(models.Record)
		fmt.Print("Для добавления новой записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Id)
		if !utils.CheckValidNumber(r.Id) {
			fmt.Println("Введенный номер не соответствует требуемому формату")
			return
		}
		if !checkAvialableNumberInFile(r.Id) {
			fmt.Print("Введите Имя: ")
			fmt.Scanln(&r.Name)
			fmt.Print("Введите адрес в формате: город улица дом квартира")
			myscanner := bufio.NewScanner(os.Stdin)
			myscanner.Scan()
			adr := strings.Split(myscanner.Text(), " ")
			if len(adr) != 4 {
				fmt.Println("Aдрес введен неверно")
			} else {
				var adres = adr[0] + " " + adr[1] + " " + adr[2] + " " + adr[3]
				r := models.Record{
					Id:     r.Id,
					Name:   r.Name,
					Adress: adres,
				}

				file, _ := os.OpenFile(nameCreatedFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
				defer file.Close()
				file.WriteString(models.RecordsToString(r))
				fmt.Println("Запись добавлена - " + models.RecordsToString(r))
			}
		} else {
			fmt.Println("Запись уже существует")
		}
	},
}

//проверка наличия записи в файле
func checkAvialableNumberInFile(id string) bool {
	file, _ := ioutil.ReadFile(nameCreatedFile)
	lines := strings.Split(string(file), "\n")
	for _, line := range lines {
		if strings.Split(line, " ")[0] == id {
			return true
		}
	}
	return false
}

func init() {
	rootCmd.AddCommand(addRecordByPhoneCmd)
}
