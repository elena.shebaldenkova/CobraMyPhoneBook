package cmd

import (
	"bufio"
	"cobraPhonebook/models"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

// метод обновляет поле адрес записи, найденной в телефонной книге по номеру телефона
var updateAdressByPhoneCmd = &cobra.Command{
	Use: "updateAdressByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		r := new(models.Record)
		fmt.Print("Для изменения существующей записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Id)
		file, _ := ioutil.ReadFile(nameCreatedFile)
		lines := strings.Split(string(file), "\n")
		for i, line := range lines {
			if strings.Split(line, " ")[0] == r.Id {
				fmt.Print("Введите новый адрес в формате: город улица дом квартира ")
				myscanner := bufio.NewScanner(os.Stdin)
				myscanner.Scan()
				adr := strings.Split(myscanner.Text(), " ")
				if len(adr) != 4 {
					fmt.Println("Aдрес введен неверно")
				} else {
					var adres = adr[0] + " " + adr[1] + " " + adr[2] + " " + adr[3]
					r := models.Record{
						Id:     r.Id,
						Name:   strings.Split(line, " ")[1],
						Adress: adres,
					}

					lines[i] = models.RecordsToString(r)
					toFile := strings.Join(lines, "\n")
					ioutil.WriteFile(nameCreatedFile, []byte(toFile), 0644)
					fmt.Println("Запись обновлена - " + models.RecordsToString(r))
					return
				}
			}
		}
		fmt.Println("Искомая запись отсутствует ")
	},
}

func init() {
	rootCmd.AddCommand(updateAdressByPhoneCmd)
}
