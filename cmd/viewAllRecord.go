package cmd

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/spf13/cobra"
)

// метод выводит все записи из файла
var viewAllRecordCmd = &cobra.Command{
	Use: "viewAllRecord",
	Run: func(cmd *cobra.Command, args []string) {
		file, _ := os.Open(nameCreatedFile)
		reader := bufio.NewReader(file)
		content, _ := ioutil.ReadAll(reader)
		fmt.Println(string(content))

	},
}

func init() {
	rootCmd.AddCommand(viewAllRecordCmd)
}
